import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { AuthService } from '../service/auth.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  cart: object;
  total : number = 0;
  user : any;
  checkoutForm = this.fb.group({
    fname: [''],
    lname: [''],
    mobile: [''],
    gender: [''],
    address:[''],
    email:[''],
    information:[''],
  })
  constructor(
    private db: AngularFirestore,
    public authService: AuthService,
    private fb: FormBuilder,
    private route: Router,
  ) {
    this.getCarts();
    this.getCurrentUser();
  }

  ngOnInit(): void {
  }

  getCarts(){
    let cartProducts : object = JSON.parse(localStorage.getItem('cart'));
    if(cartProducts){
      this.cart = cartProducts;
      for (const [key, product] of Object.entries(cartProducts)) {
        this.total += product.price*product.stock;
      };
    }
  }

  getCurrentUser(){
    this.user = this.authService.userInfo();
    console.log('tes');
    console.log(this.user);
    this.checkoutForm.controls['fname'].setValue(this.user.fname);
    this.checkoutForm.controls['lname'].setValue(this.user.lname);
    this.checkoutForm.controls['mobile'].setValue(this.user.mobile);
    this.checkoutForm.controls['gender'].setValue(this.user.gender);
    this.checkoutForm.controls['address'].setValue(this.user.address);
    this.checkoutForm.controls['email'].setValue(this.user.email);
    // this.user = currentUser
  }

  placeOrder(informacione){
    let orderDoc = this.db.firestore.collection('orders').orderBy('id', 'desc').limit(1);
    orderDoc.get().then((querySnapshot) => {
      informacione.products = this.cart;
      informacione.total = this.total;
      informacione.date = Math.floor(Date.now() / 1000);
      if(querySnapshot.size){
        querySnapshot.forEach(lastorder => {
          let lastId = (lastorder.id) ? lastorder.id : '0';
          let nextId = parseFloat(lastId)+1;
          informacione.id = nextId;
          localStorage.setItem('cart', JSON.stringify({}));
          this.db.collection('orders').doc(''+nextId).set(informacione);
          this.route.navigate(['home']);
        });
      } else {
        let lastId = '0';
        let nextId = parseFloat(lastId)+1;
        informacione.id = nextId;
        localStorage.setItem('cart', JSON.stringify({}));
        this.db.collection('orders').doc(''+nextId).set(informacione);
        this.route.navigate(['home']);
      }
    });
  }
}
