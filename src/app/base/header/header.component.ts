import { Component, OnInit, ViewEncapsulation  } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from '../../service/auth.service';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class HeaderComponent implements OnInit {
  userAvatar: string;
  miniCart: object;
  userRole: boolean = false;
  subtotal : number = 0;
  shipping : number = 0;
  total : number;
  faShoppingCart = faShoppingCart;
  faTimes = faTimes;
  faUser = faUser;
  constructor(
    public authService: AuthService,
    public auth: AngularFireAuth,
    private db: AngularFirestore
  ){
    this.getCarts();
    console.log(this.authService.userInfo());
    this.userAvatar = localStorage.getItem('userInic');
    this.auth.authState.subscribe(res => {
      if (res && res.uid) {
        this.db.collection('users').doc(res.uid).valueChanges().subscribe((users : any) => {
          console.log(users);
          if(users.role==1){
            this.userRole = true;
          }
        });
      }
    });
  }

  ngOnInit(): void {
  }

  getCarts(){
    let cartProducts : object = JSON.parse(localStorage.getItem('cart'));
    if(cartProducts){
      this.miniCart = cartProducts;
      for (const [key, product] of Object.entries(cartProducts)) {
        this.subtotal += product.price;
      };
      this.total = this.subtotal + this.shipping;
    }
    // console.log(this.miniCart);
    // this.miniCart = {"id1":{"id":1,"categoryId":1,"image":"","name":"Scanpan Classic Covered Saute Pan 26cm","category":"Graphic Corner","price":68.3,"stock":2},"id101":{"image":"","id":101,"category":"Graphic Corner","categoryId":1,"price":68.3,"name":"Scanpan Classic Covered Saute Pan 26cm","stock":1}};
  }

  removeCart(id){
    let currCart : object = JSON.parse(localStorage.getItem('cart'));
    delete currCart['id'+id];
    document.querySelector("#minicart-product-"+id).remove();
    let cartPage = document.querySelector("#cart-product-"+id);
    if(cartPage){
      cartPage.remove();
    }
    this.total = 0;
    for (const [key, product] of Object.entries(currCart)) {
      this.total += product.price;
    };
    localStorage.setItem('cart', JSON.stringify(currCart));
  }

  logout() {
    this.authService.logout();
  }

  objectchange(){
    console.log('chage')
  }

}
