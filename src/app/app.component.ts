import { Component } from '@angular/core';
import { AuthService } from './service/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-bootstrap';
  email: string;
  password: string;
  user: any;
  isLogged: boolean = false;
  cart: object;

  constructor(public authService: AuthService) {
    if(authService.isLogged()){
      this.isLogged = true;
    }
    this.getCarts();
  }

  logout() {
    this.authService.logout();
  }

  getCarts(){
    this.cart = JSON.parse(localStorage.getItem('cart'));
    // console.log(this.miniCart);
    // this.miniCart = {"id1":{"id":1,"categoryId":1,"image":"","name":"Scanpan Classic Covered Saute Pan 26cm","category":"Graphic Corner","price":68.3,"stock":2},"id101":{"image":"","id":101,"category":"Graphic Corner","categoryId":1,"price":68.3,"name":"Scanpan Classic Covered Saute Pan 26cm","stock":1}};
  }
}
