import { Component, OnInit } from '@angular/core';
import { faChevronUp } from '@fortawesome/free-solid-svg-icons';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  miniCart: object;
  subtotal : number = 0;
  shipping : number = 0;
  total : number;
  faChevronUp = faChevronUp;
  faChevronDown = faChevronDown;
  faTimes = faTimes;
  constructor() {
    this.getCarts();
  }

  ngOnInit(): void {
  }

  getCarts(){
    let cartProducts : object = JSON.parse(localStorage.getItem('cart'));
    if(cartProducts){
      this.miniCart = cartProducts;
      for (const [key, product] of Object.entries(cartProducts)) {
        this.subtotal += product.price;
      };
      this.total = this.subtotal + this.shipping;
    }
    // console.log(this.miniCart);
    // this.miniCart = {"id1":{"id":1,"categoryId":1,"image":"","name":"Scanpan Classic Covered Saute Pan 26cm","category":"Graphic Corner","price":68.3,"stock":2},"id101":{"image":"","id":101,"category":"Graphic Corner","categoryId":1,"price":68.3,"name":"Scanpan Classic Covered Saute Pan 26cm","stock":1}};
  }

  removeCart(id){
    let currCart : object = JSON.parse(localStorage.getItem('cart'));
    delete currCart['id'+id];
    document.querySelector("#minicart-product-"+id).remove();
    document.querySelector("#cart-product-"+id).remove();
    this.total = 0;
    for (const [key, product] of Object.entries(currCart)) {
      this.total += product.price;
    };
    localStorage.setItem('cart', JSON.stringify(currCart));
  }

  addQty(id){
    let inputValue = (document.querySelector("#cart-product-"+id+" .product-qty") as HTMLInputElement);
    let newInput : number = parseInt(inputValue.value)+1;
    inputValue.value = ''+newInput;
    let cartProducts : object = JSON.parse(localStorage.getItem('cart'));
    if(cartProducts){
      cartProducts['id'+id].stock = newInput;
    }
    localStorage.setItem('cart', JSON.stringify(cartProducts));
    let priceValue = (document.querySelector("#cart-product-"+id+" .whish-list-price span") as HTMLInputElement);
    priceValue.innerHTML = ''+(newInput*parseFloat(cartProducts['id'+id].price));
    this.total += parseFloat(cartProducts['id'+id].price);
  }
  removeQty(id){
    let inputValue = (document.querySelector("#cart-product-"+id+" .product-qty") as HTMLInputElement);
    let newInput = parseInt(inputValue.value)-1;
    if(newInput>0){
      inputValue.value = ''+newInput;
      let cartProducts : object = JSON.parse(localStorage.getItem('cart'));
      if(cartProducts){
        cartProducts['id'+id].stock = newInput;
      }
      localStorage.setItem('cart', JSON.stringify(cartProducts));
      let priceValue = (document.querySelector("#cart-product-"+id+" .whish-list-price span") as HTMLInputElement);
      priceValue.innerHTML = ''+(parseFloat(cartProducts['id'+id].stock)*parseFloat(cartProducts['id'+id].price));
      this.total -= parseFloat(cartProducts['id'+id].price);
    }
  }

}
