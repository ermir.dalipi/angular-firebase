export interface ProductModel {
    id?: number;
    name?: string;
    category: string;
    categoryId?: number;
    image?: string;
    price?: number;
    stock?: number;
}