export interface OrderModel {
    id: number;
    email: string;
    fname: string;
    lname: string;
    information: string;
    mobile: number;
    total: number;
    date: any;
    dateF: string;
    products: object;
}