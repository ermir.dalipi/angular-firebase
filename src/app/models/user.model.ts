export interface UserModel {
  uid: string;
  fname: string;
  lname: string;
  email: string;
  mobile: number;
  address: string;
  gender: string;
  role: number;
}
