import { Component, OnInit, Input, ElementRef  } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ProductModel } from '../../models/product.model';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  @Input() product: any;
  nextProd: number;
  products: object;

  
  constructor(
    private db: AngularFirestore,
    private elementRef:ElementRef
  ) {
    // this.getAllProducts();
    console.log(this.product);
  }

  ngOnInit(): void {
  }

  addToCart(prodId){
    
  }

  async getAllProducts(){
    await this.db.collection('products').snapshotChanges().forEach(products => {
      // console.log(products);
      this.products = products;
    })
  }

  async addProducts(){
    const singleProduct = {
      id: 1,
      name : 'Scanpan Classic Covered Saute Pan 26cm',
      image : 'https://htmldemo.hasthemes.com/benito/benito/assets/images/products/product1.jpg',
      category : 'Graphic Corner',
      categoryId : 1,
      price : 68.30
    }
    // this.db.collection('products').doc('2').set(singleProduct);
    // this.db.collection("products").orderBy().get();
    let lastProd = this.db.collection('products', ref => ref.orderBy('key'));

    // await this.db.collection('products', ref => ref.orderBy('id', 'desc').limit(1)).valueChanges().subscribe((lastProd) => {
    //   let lastProdId : any = lastProd[0];
    //   let nextProd : number = lastProdId.id + 1;
    //   let nextProdS : string = ''+nextProd;
    //   singleProduct.id = nextProd;
    //   this.nextProd = nextProd
    //   // this.db.collection('products').doc(nextProdS).set(singleProduct);
    //   console.log(this.nextProd);
    // });


    // let events: any = await this.db.collection('products', ref => ref.orderBy('id', 'desc').limit(1)).snapshotChanges().subscribe(res =>(console.log(res)));
    // console.log(events.data())
    // console.log(getDatas.docs.map(doc => doc.data()));
    // console.log(getDatas.data());
    // await this.db.collection('products', ref => ref.orderBy('id', 'desc').limit(1)).get().subscribe((lastProd) => {
    //   let lastProdId : any = lastProd[0];
    //   let nextProd : number = lastProdId.id + 1;
    //   let nextProdS : string = ''+nextProd;
    //   singleProduct.id = nextProd;
    //   this.nextProd = nextProd
    //   // this.db.collection('products').doc(nextProdS).set(singleProduct);
    //   console.log(this.nextProd);
    // });
    
    console.log(lastProd);
    // this.db.collection("products").orderBy('name').limit(3).get();
    // db.collection("cities").doc("LA").set(singleProduct);
  }

  addProductsCart(id){
    let allProducts : object = JSON.parse(localStorage.getItem('cart'));
    console.log(allProducts);
    if(!allProducts){
      allProducts = {}
    }
    let idString : string = ''+id;
    let thisProduct = this.db.collection('products').doc('1').snapshotChanges();
    console.log(thisProduct)
    this.db.collection('products').doc(idString).valueChanges().subscribe((product) => {
      // console.log(product)
    });
    let userDoc = this.db.firestore.collection('products').doc(idString);
    userDoc.get().then((querySnapshot) => {
      console.log(querySnapshot.data());  
      let currProd = querySnapshot.data();
      // querySnapshot.forEach((doc) => {
      //   console.log(doc.id, "=>", doc.data());  
      // })
      // allProducts.push(currProd)
      // Object.assign(allProducts, currProd)
      // console.log(currProd);
      // allProducts = {...allProducts, ...currProd};
      // console.log(allProducts);
      // allProducts.push(currProd);
      // allProducts.forEach((product)=>{
      //   interface Provider {
      //     id: number;
      //     name: string;
      //     category: string;
      //     categoryId: number;
      //     image: string;
      //     price: number;
      //     stock: number;
      //   }
      //   let thisProducts : Array<Provider>;
      //   thisProducts = product;
      //   console.log(thisProducts.id);
      // });
      // let newProd : object[] = [currProd];


      if(!allProducts['id'+currProd.id]){
        allProducts['id'+currProd.id] = currProd;
        allProducts['id'+currProd.id]['stock'] = 1;
        let miniCartElem = document.querySelector('.checkout-scroll');
        let html = '<li class="checkout-cart-list" id="minicart-product-'+currProd.id+'">';
        html += '<div class="checkout-img"><img src="https://picsum.photos/id/'+currProd.id+'/85/85" alt="img" class="product-image"><span class="product-quantity">1</span></div>';
        html += '<div class="checkout-block">';
          html += '<a class="product-name" href="/product/'+currProd.id+'">'+currProd.name+'</a><span class="product-price">$'+currProd.price+'</span>';
          html += '<a class="remove-cart" data-id="'+currProd.id+'">';
            html += '<fa-icon class="ng-fa-icon" ng-reflect-icon="[object Object]"><svg role="img" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="times" class="svg-inline--fa fa-times fa-w-11" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512"><path fill="currentColor" d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path></svg></fa-icon>';
          html += '</a>';
        html += '</div></li>';
        html += '</li>';
        // miniCartElem.insertAdjacentHTML('beforeend', html);
        let miniCartElems = document.querySelector('.checkout-scroll');
        // var compiledHtml = $compile(html);
        miniCartElems.insertAdjacentHTML('beforeend', html);
        localStorage.setItem('cart', JSON.stringify(allProducts));
        let thisMiniCart: HTMLElement = document.querySelector('#minicart-product-'+currProd.id+' .remove-cart') as HTMLElement;
        // thisMiniCart.onclick = this.removeCart()
        console.log(thisMiniCart.onclick = this.removeCart)
        let subtotal = document.querySelector('.total-price');
        let cartTotal = 0;
        for (const [key, product] of Object.entries(allProducts)) {
          cartTotal += product.price;
        };
        subtotal.innerHTML = ''+cartTotal;
        // subtotal.innerHTML = parseFloat(subtotal.innerHTML) + currProd.price;
        // console.log(subtotal);
        // thisMiniCart.onclick = this.removeCart(currProd.id)
        //  = this.removeCart(1);
        // thisMiniCart.click();

        
      }
    });
  }

  removeCart(cid){
    console.log(cid.target.getAttribute('data-id'));
    let id = cid.target.getAttribute('data-id');
    let currCart : object = JSON.parse(localStorage.getItem('cart'));
    delete currCart['id'+id];
    let subtotal = document.querySelector('.total-price');
    let cartSubtotal = 0;
    for (const [key, product] of Object.entries(currCart)) {
      cartSubtotal += product.price;
    };
    subtotal.innerHTML = ''+cartSubtotal;
    document.querySelector("#minicart-product-"+id).remove();
    localStorage.setItem('cart', JSON.stringify(currCart));
  }

  async addProductDb(){

  }

}
