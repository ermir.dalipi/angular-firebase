import { Component, OnInit } from '@angular/core';
import { PostsComponent } from '../components/posts/posts.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  products: Object[];
  constructor(
    private db: AngularFirestore
  ) {
    this.getAllProducts();
    console.log(this.products)
  }

  ngOnInit(): void {
  }

  p: number = 1;
  collection: any[] = [1,2,3,4,5,6,7,8,9];

  getAllProducts(){
    this.db.collection('products').valueChanges().subscribe(products => {
      this.products = products;
      // console.log(products);
    })
  }

}
