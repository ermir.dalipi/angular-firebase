import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import {NgxPaginationModule} from 'ngx-pagination';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder } from '@angular/forms';
import { AuthService } from '../../service/auth.service';
import { UserModel } from '../../models/user.model'

@Component({
  selector: 'app-alluser',
  templateUrl: './alluser.component.html',
  styleUrls: ['./alluser.component.css']
})
export class AlluserComponent implements OnInit {
  users: object;
  cuser: UserModel;
  p: number = 1;
  closeResult: string;
  edituserForm = this.fb.group({
    fname: [''],
    lname: [''],
    password: [''],
    mobile: [''],
    gender: [''],
    address:[''],
    role:['']
  })
  constructor(
    public authService: AuthService,
    private db: AngularFirestore,
    private modalService: NgbModal,
    private fb: FormBuilder,
  ) {
    this.allUsers();
    this.currentUser();
  }

  ngOnInit(): void {

  }

  async currentUser(){
    let thisUser : UserModel = this.authService.userInfo();
    this.cuser = thisUser;
    console.log(thisUser.role);
  }

  allUsers(){
    this.db.collection('users').valueChanges().subscribe((users) => {
      this.users = users;
    });
  }

  showModal(id){
    let userDoc = this.db.firestore.collection('users').doc(id);
    userDoc.get().then((querySnapshot) => {
      console.log(querySnapshot.data());  
      let currUser = querySnapshot.data();
      let myModal = document.getElementById('showProfile');
      console.log(myModal);
      myModal.addEventListener('shown.bs.modal', function () {
      });
    });
  }

  open(content, id, name) {
    let userDoc = this.db.firestore.collection('users').doc(id);
    userDoc.get().then((querySnapshot) => {
      console.log(querySnapshot.data());  
      let currUser = querySnapshot.data();
      console.log(name)
      if(name == 'edit'){
        this.edituserForm.controls['fname'].setValue(currUser.fname);
        this.edituserForm.controls['lname'].setValue(currUser.lname);
        this.edituserForm.controls['mobile'].setValue(currUser.mobile);
        this.edituserForm.controls['gender'].setValue(currUser.gender);
        this.edituserForm.controls['address'].setValue(currUser.address);
        this.edituserForm.controls['role'].setValue(currUser.role);
      } else {
        let modalTitle = document.querySelector('#showModalTitle span');
        modalTitle.innerHTML = currUser.fname+' '+currUser.lname;
        let modalFname = document.querySelector('#showModalInfoFname');
        modalFname.innerHTML = currUser.fname;
        let modalLname = document.querySelector('#showModalInfoLname');
        modalLname.innerHTML = currUser.lname;
        let modalEmail = document.querySelector('#showModalInfoEmail');
        modalEmail.innerHTML = currUser.email;
        let modalGender = document.querySelector('#showModalInfoGender');
        modalGender.innerHTML = currUser.gender;
        let modalMobile = document.querySelector('#showModalInfoMobile');
        modalMobile.innerHTML = currUser.gender;
        let modalAddress = document.querySelector('#showModalInfoAddress');
        modalAddress.innerHTML = currUser.address;
        let modalRole = document.querySelector('#showModalInfoRole');
        modalRole.innerHTML = (currUser.role == 1) ? 'Menaxher' : 'Perdorues';
      }
    });
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      console.log(result);
    }, (reason) => {
      // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  editProfile(content, id){
    let userDoc = this.db.firestore.collection('users').doc(id);
    userDoc.get().then((querySnapshot) => {
      console.log(querySnapshot.data());  
      let currUser = querySnapshot.data();
      this.edituserForm.controls['fname'].setValue(currUser.fname);
      this.edituserForm.controls['lname'].setValue(currUser.lname);
      this.edituserForm.controls['mobile'].setValue(currUser.mobile);
      this.edituserForm.controls['gender'].setValue(currUser.gender);
      this.edituserForm.controls['address'].setValue(currUser.address);
    });
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      console.log(result);
    }, (reason) => {
      // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  updateUser(user) {
    console.log(user);
    this.authService.updateUser(user);
    // this.db.collection('users').add(user);
    // user.email = user.password = '';
    this.modalService.dismissAll();
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
