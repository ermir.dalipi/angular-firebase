import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import {NgxPaginationModule} from 'ngx-pagination';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder } from '@angular/forms';
import { AuthService } from '../../service/auth.service';
import { OrderModel } from '../../models/order.model';
import { ProductModel } from '../../models/product.model';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  users: object;
  orders: object;
  cuser : any;
  p: number = 1;
  closeResult: string;
  edituserForm = this.fb.group({
    fname: [''],
    lname: [''],
    password: [''],
    mobile: [''],
    gender: [''],
    address:['']
  })
  constructor(
    public authService: AuthService,
    private db: AngularFirestore,
    private modalService: NgbModal,
    private fb: FormBuilder,
  ) {
    this.allOrders();
    this.currentUser();
  }

  ngOnInit(): void {

  }

  async currentUser(){
    let thisUser : any = this.authService.userInfo();
    this.cuser = thisUser;
  }

  allOrders(){
    this.db.collection('orders').valueChanges().subscribe((orders) => {
      console.log(orders)
      let allOrders : object[];
      allOrders = [];
      orders.forEach((order : OrderModel) => {
        let currOrder : OrderModel = order;
        if(currOrder.email == this.cuser.email){
          let newDate = new Date(currOrder.date.seconds*1000);
          let newDateF = newDate.getDate()+'.'+ newDate.getMonth()+'.'+newDate.getFullYear();
          currOrder.dateF = newDateF;
          allOrders.push(currOrder);
        }
      });
      this.users = allOrders;
      this.orders = allOrders;
    });
  }

  showModal(id){
    let userDoc = this.db.firestore.collection('users').doc(id);
    userDoc.get().then((querySnapshot) => {
      console.log(querySnapshot.data());  
      let currUser = querySnapshot.data();
      let myModal = document.getElementById('showProfile');
      console.log(myModal);
      myModal.addEventListener('shown.bs.modal', function () {
      });
    });
  }

  open(content, id, name) {
    console.log(id);
    let ordersDoc = this.db.firestore.collection('orders').doc(''+id);
    ordersDoc.get().then((querySnapshot) => {
      console.log(querySnapshot.data());  
      let currOrder = querySnapshot.data();
      console.log(name)
      let modalFname = document.querySelector('#showModalInfoFname');
        modalFname.innerHTML = currOrder.fname;
        let modalLname = document.querySelector('#showModalInfoLname');
        modalLname.innerHTML = currOrder.lname;
        let modalEmail = document.querySelector('#showModalInfoEmail');
        modalEmail.innerHTML = currOrder.email;
        let modalMobile = document.querySelector('#showModalInfoMobile');
        modalMobile.innerHTML = currOrder.mobile;
        let modalAddress = document.querySelector('#showModalInfoAddress');
        modalAddress.innerHTML = currOrder.address;
        let html = '';
        Object.values(currOrder.products).forEach((product : ProductModel) => {
          html += '<tr>';
          html += '<td><img src="https://picsum.photos/id/'+product.id+'/50/50"></td>';
          html += '<td>'+product.name+'</td>';
          html += '<td>'+product.stock+'</td>';
          html += '<td>'+(product.price * product.stock)+'</td>';
          html += '</tr>';
        });
        let modalProducts = document.querySelector('#productBody');
        modalProducts.innerHTML = html;
    });
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      console.log(result);
    }, (reason) => {
      // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  updateUser(user) {
    console.log(user);
    this.authService.updateUser(user);
    // this.db.collection('users').add(user);
    // user.email = user.password = '';
    this.modalService.dismissAll();
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
