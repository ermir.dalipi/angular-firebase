import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder } from '@angular/forms';
import { AuthService } from '../../service/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm = this.fb.group({
    fname: [''],
    lname: [''],
    email: [''],
    password: [''],
    mobile: [''],
    gender: [''],
    address:['']
  })

  constructor(
    private fb: FormBuilder,  
    private db: AngularFirestore,
    public authService: AuthService
  ) { }

  ngOnInit() {
  }

  // createUser(user) {
  //   console.log(user)
  //   this.db.collection('Users').add(user);
  // }

  createUser(user) {
    this.authService.signup(user);
    // this.db.collection('users').add(user);
    // user.email = user.password = '';
  }

}
