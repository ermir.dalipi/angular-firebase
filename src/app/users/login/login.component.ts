import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { UserModel } from '../../models/user.model';
import { AuthService } from '../../service/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  allUsers: UserModel;
  isCorrect: boolean = null;
  firebaseUsers;

  constructor(
    private route: Router,
    // private authService: AuthService,
    private db: AngularFirestore,
    public authService: AuthService,
    public auth: AngularFireAuth
  ) {
    console.log(authService.isLogged())
  }

  ngOnInit() {
  }

  login(formValues) {
    this.authService.login(formValues.email, formValues.password);
    formValues.email = formValues.password = '';    
  }


}
