import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import {NgxPaginationModule} from 'ngx-pagination';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder } from '@angular/forms';
import { AuthService } from '../../service/auth.service';

@Component({
  selector: 'app-myproducts',
  templateUrl: './myproducts.component.html',
  styleUrls: ['./myproducts.component.css']
})
export class MyproductsComponent implements OnInit {
  products: object;
  p: number = 1;
  closeResult: string;
  addproductForm = this.fb.group({
    name: [''],
    description: [''],
    category: [''],
    categoryId: [''],
    price: [''],
    stock: [''],
  })
  editproductForm = this.fb.group({
    name: [''],
    description: [''],
    category: [''],
    categoryId: [''],
    price: [''],
    stock: [''],
  })
  constructor(
    public authService: AuthService,
    private db: AngularFirestore,
    private modalService: NgbModal,
    private fb: FormBuilder,
  ) {
    this.allProducts();
  }

  ngOnInit(): void {
  }

  allProducts(){
    this.db.collection('products').valueChanges().subscribe((products) => {
      // console.log(products)
      this.products = products;
    });
  }

  open(content, id, name) {
    if(name != 'add') {
      let productDoc = this.db.firestore.collection('products').doc(''+id);
      productDoc.get().then((querySnapshot) => {
        console.log(querySnapshot.data());  
        let currProd = querySnapshot.data();
        if(name == 'edit'){
          console.log(id);
          this.editproductForm.controls['name'].setValue(currProd.name);
          this.editproductForm.controls['description'].setValue(currProd.description);
          this.editproductForm.controls['categoryId'].setValue(currProd.categoryId);
          this.editproductForm.controls['price'].setValue(currProd.price);
          this.editproductForm.controls['stock'].setValue(currProd.stock);
        } else if(name == 'delete'){
          let modalTitle = document.querySelector('#showModalTitle span');
          modalTitle.innerHTML = currProd.name;
          let modalId = document.querySelector('#deleteProduct');
          console.log(modalId);
          modalId.setAttribute('data-id', currProd.id);
        } else {
          let modalImage = document.querySelector('#showModalInfoImage');
          modalImage.innerHTML = '<img src="https://picsum.photos/id/'+currProd.id+'/160/160">';
          let modalName = document.querySelector('#showModalInfoName');
          modalName.innerHTML = currProd.name;
          let modalDesc = document.querySelector('#showModalInfoDesc');
          modalDesc.innerHTML = currProd.name;
          let modalCategory = document.querySelector('#showModalInfoCategory');
          modalCategory.innerHTML = currProd.category;
          let modalPrice = document.querySelector('#showModalInfoPrice');
          modalPrice.innerHTML = currProd.price;
          let modalStock = document.querySelector('#showModalInfoStock');
          modalStock.innerHTML = currProd.stock;
        }
      });
    }
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      console.log(result);
    }, (reason) => {
      // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  editProfile(content, id){
    // let userDoc = this.db.firestore.collection('users').doc(id);
    // userDoc.get().then((querySnapshot) => {
    //   console.log(querySnapshot.data());  
    //   let currUser = querySnapshot.data();
    //   this.edituserForm.controls['fname'].setValue(currUser.fname);
    //   this.edituserForm.controls['lname'].setValue(currUser.lname);
    //   this.edituserForm.controls['mobile'].setValue(currUser.mobile);
    //   this.edituserForm.controls['gender'].setValue(currUser.gender);
    //   this.edituserForm.controls['address'].setValue(currUser.address);
    // });
    // this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
    //   this.closeResult = `Closed with: ${result}`;
    //   console.log(result);
    // }, (reason) => {
    //   // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    // });
  }

  addProductDb(product){
    let productDoc = this.db.firestore.collection('products').orderBy('id', 'desc').limit(1);
    productDoc.get().then((querySnapshot) => {
      querySnapshot.forEach(lastprod => {
        let lastId = lastprod.id;
        let nextId = parseFloat(lastId)+1;
        if(product.categoryId == 2){
          product.category = 'Makina & Motorra';
        } else if(product.categoryId == 3){
          product.category = 'Elektronike';
        } else {
          product.category = 'Libra';
        }
        product.id = nextId;
        this.db.collection('products').doc(''+nextId).set(product);
        this.modalService.dismissAll();
      })
    });
  }

  updateProduct(product) {
    console.log(product);
    this.db.collection('products').doc(''+product.id).update(product);
    this.modalService.dismissAll();
    // this.authService.updateUser(user);
    // this.db.collection('users').add(user);
    // user.email = user.password = '';
  }

  delteProduct(){
    let modalId = document.querySelector('#deleteProduct');
    console.log();
    let prodId = modalId.getAttribute('data-id');
    this.db.collection('products').doc(''+prodId).delete();
    this.modalService.dismissAll();
  }
  

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}
