import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../service/auth.service';
import { FormBuilder } from '@angular/forms';
import { UserModel } from '../../models/user.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  user : any;
  cuser : any;
  profileForm = this.fb.group({
    fname: [''],
    lname: [''],
    password: [''],
    mobile: [''],
    gender: [''],
    address:['']
  })
  constructor(
    public authService: AuthService,
    private fb: FormBuilder,  
  ) {
    this.getCurrentUser();
    this.currentUser();
    
  }

  ngOnInit(): void {
  }

  async currentUser(){
    let thisUser : UserModel = this.authService.userInfo();
    this.cuser = thisUser;
    console.log(thisUser.role);
  }

  getCurrentUser(){
    this.user = this.authService.userInfo();
    if(this.user){
      console.log('tes');
      console.log(this.user);
      this.profileForm.controls['fname'].setValue(this.user.fname);
      this.profileForm.controls['lname'].setValue(this.user.lname);
      this.profileForm.controls['mobile'].setValue(this.user.mobile);
      this.profileForm.controls['gender'].setValue(this.user.gender);
      this.profileForm.controls['address'].setValue(this.user.address);
      // this.user = currentUser
    }
  }

  updateUser(user) {
    this.authService.updateUser(user);
    // this.db.collection('users').add(user);
    user.email = user.password = '';
  }

}
