import { Injectable, NgZone } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { UserModel } from '../models/user.model';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userData: any;
  
  constructor(
    private firebaseAuth: AngularFireAuth,
    private db: AngularFirestore,
    private route: Router,
  ) {
    this.userInfo();
  }

  isLogged(): boolean {
    this.firebaseAuth.authState.subscribe(res => {
      if (res && res.uid) {
        // console.log('user is logged in');
        // console.log(res);
        // return true;
        this.db.collection('users').doc(res.uid).valueChanges().subscribe((user : UserModel) => {
          // console.log(user)
          return {
            userId:  res.uid,
            userMail:  res.email,
            userFirstName:  user.fname,
            userLastName:  user.lname,
            userMobile:  user.mobile,
            userAddress:  user.address,
            userGender:  user.gender,
            userRole:  user.role
          }
        });
      }
    });
    return false;
  }

  userInfo() {
    let userTooken = localStorage.getItem('useruid');
    if(userTooken){
      if(!this.userData){
        let userEmail = localStorage.getItem('userMail');
        this.db.collection('users').doc(userTooken).valueChanges().subscribe((user : UserModel) => {
          let currentUsers : object = {
            userId:  userTooken,
            email:  userEmail,
            fname:  user.fname,
            lname:  user.lname,
            mobile:  user.mobile,
            address:  user.address,
            gender:  user.gender,
            role:  user.role
          }
          console.log(user);
          this.userData = currentUsers;
          return currentUsers;
        });
      } else {
        return this.userData;
      }
      // await this.firebaseAuth.authState.forEach(res=>{
      //   if (res && res.uid) {
      //     this.db.collection('users').doc(res.uid).valueChanges().subscribe((user : UserModel) => {
      //       // return users;
      //       let currentUsers : object = {
      //         userId:  res.uid,
      //         userMail:  res.email,
      //         userFirstName:  user.fname,
      //         userLastName:  user.lname,
      //         userMobile:  user.mobile,
      //         userAddress:  user.address,
      //         userGender:  user.gender,
      //         userRole:  user.role
      //       }
      //       this.userData = currentUsers;
      //       return currentUsers;
      //     });
      //   }
      // });
    } else {
      return {login: 'false'};
    }
    // let userInfo =  await this.firebaseAuth.authState.subscribe(res => {
    //   if (res && res.uid) {
    //     this.db.collection('users').doc(res.uid).valueChanges().subscribe((user : UserModel) => {
    //       // return users;
    //       let currentUsers : object = {
    //         userId:  res.uid,
    //         userMail:  res.email,
    //         userFirstName:  user.fname,
    //         userLastName:  user.lname,
    //         userMobile:  user.mobile,
    //         userAddress:  user.address,
    //         userGender:  user.gender,
    //         userRole:  user.role
    //       }
    //       this.userData = currentUsers;
    //       console.log(currentUsers);
    //     });
    //     return {status: 'bosh1'};
    //   }
    //   return {status: 'bosh2'};
    // });
    // return {status: 'bosh4'};
  }


  async login(email: string, password: string) {
    this.firebaseAuth
      .signInWithEmailAndPassword(email, password)
      .then(value => {
        this.addWithAsync(value.user.uid);
        localStorage.setItem('userMail', email);
        this.route.navigate(['home']);
      })
      .catch(err => {
        console.log('Something went wrong:',err.message);
      });
  }

  async addWithAsync(userUid: string) {
    this.db.collection('users').doc(userUid).valueChanges().subscribe((users) => {
      this.userData = users;
      let userInfo : any = users;
      let token: any = Math.random().toString(36).substr(2)+Math.random().toString(36).substr(2);
      localStorage.setItem('useruid', userUid);
      localStorage.setItem('userName', userInfo.fname+' '+userInfo.lname);
      localStorage.setItem('userInic', userInfo.fname.charAt(0)+userInfo.lname.charAt(0));
    })
  }
  

  signup(user: any) {
    this.firebaseAuth
      .createUserWithEmailAndPassword(user.email, user.password)
      .then(value => {
        user.uid = value.user.uid;
        user.role = 2;
        this.db.collection('users').doc(value.user.uid).set(user);
        localStorage.setItem('useruid', value.user.uid);
        localStorage.setItem('userName', user.fname+' '+user.lname);
        localStorage.setItem('userMail', value.user.email);
        localStorage.setItem('userInic', user.fname.charAt(0)+user.lname.charAt(0));
        this.route.navigate(['home']);
      })
      .catch(err => {
        console.log('Something went wrong:',err.message);
      });
  }

  updateUser(user: any){
    console.log(user);
    console.log(this.userData);
    this.db.collection('users').doc(this.userData.userId).update(user);
    console.log(this.userData);
  }

  logout() {
    this.firebaseAuth.signOut();
    window.location.reload();
  }
}
