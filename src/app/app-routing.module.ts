import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AngularFireAuthGuard } from '@angular/fire/auth-guard';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './users/login/login.component';
import { RegisterComponent } from './users/register/register.component';
import { ProductComponent } from './product/product.component';
import { ProfileComponent } from './users/profile/profile.component';
import { AlluserComponent } from './users/alluser/alluser.component';
import { MyproductsComponent } from './users/myproducts/myproducts.component';
import { CartComponent } from './cart/cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { OrdersComponent } from './users/orders/orders.component';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'login', component: LoginComponent}, 
  {path: 'register', component: RegisterComponent},
  {path: 'product/:id', component: ProductComponent},
  {path: 'cart', component: CartComponent},
  {path: 'checkout', component: CheckoutComponent, canActivate: [AngularFireAuthGuard]},
  {path: 'profile', component: ProfileComponent, canActivate: [AngularFireAuthGuard] },
  {path: 'allusers', component: AlluserComponent, canActivate: [AngularFireAuthGuard] },
  {path: 'myproducts', component: MyproductsComponent, canActivate: [AngularFireAuthGuard] },
  {path: 'orders', component: OrdersComponent, canActivate: [AngularFireAuthGuard] },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
